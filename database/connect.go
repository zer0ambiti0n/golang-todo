package database

import (
	"fmt"
	"strconv"

	"golang-todo/config"

	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
)

func ConnectDB() {
	var err error
	p := config.Env("DB_PORT")
	port, err := strconv.ParseUint(p, 10, 32)

	configData := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		config.Env("DB_HOST"),
		port,
		config.Env("DB_USERNAME"),
		config.Env("DB_PASSWORD"),
		config.Env("DB_DATABASE"),
	)

	DB, err = gorm.Open(
		"postgres",
		configData,
	)

	if err != nil {
		fmt.Println(
			err.Error(),
			configData,
		)
		panic("failed to connect database")
	}

	fmt.Println("Connection Opened to Database")
}
