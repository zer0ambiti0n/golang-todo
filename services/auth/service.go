package auth

import (
	"errors"
	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
	"golang-todo/config"
	"golang-todo/models"
	"golang-todo/services/user"
	"golang.org/x/crypto/bcrypt"
	"strconv"
	"time"
)

type RegisterRequest struct {
	Email                string `json:"email" validate:"required,email"`
	Name                 string `json:"name" validate:"required"`
	Password             string `json:"password" validate:"required,min=5"`
	PasswordConfirmation string `json:"passwordConfirmation" validate:"required,eqfield=Password"`
}

type LoginRequest struct {
	Email    string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required"`
}

type AuthService struct {
	userRepository *user.UserRepository
}

func NewAuthService(userRepository *user.UserRepository) *AuthService {
	return &AuthService{
		userRepository: userRepository,
	}
}

func (service *AuthService) Register(dto RegisterRequest) (models.User, error) {
	userModel := models.User{
		Email:       dto.Email,
		Name:        dto.Name,
		Description: "",
		Status:      models.ACTIVE,
		Password:    []byte(dto.Password),
	}
	_, err := service.userRepository.FindByEmail(dto.Email)
	if err == nil {
		return userModel, errors.New("User already exists")
	}

	userModel, err = service.userRepository.Create(userModel)

	return userModel, err
}

func (service *AuthService) Login(dto LoginRequest) (models.User, error) {
	userModel, err := service.userRepository.FindByEmail(dto.Email)
	if err != nil {
		return userModel, err
	}

	if err := bcrypt.CompareHashAndPassword(userModel.Password, []byte(dto.Password)); err != nil {
		return userModel, errors.New("User not found!")
	}

	return userModel, nil
}

func (service *AuthService) CreateJWTToken(user models.User, c *fiber.Ctx) (string, int64, error) {
	exp := time.Now().Add(time.Minute * 40)
	claims := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
		Issuer:    strconv.Itoa(int(user.ID)),
		ExpiresAt: exp.Unix(),
	})

	token, err := claims.SignedString([]byte(config.Env("JWT_SECRET")))
	if err != nil {
		return "", 0, err
	}

	cookie := fiber.Cookie{
		Name:     "jwt",
		Value:    token,
		Expires:  exp,
		HTTPOnly: true,
	}

	c.Cookie(&cookie)

	return token, exp.Unix(), nil
}

func (service *AuthService) GetAuthUser(cookie string) (models.User, error) {
	var model models.User

	token, err := jwt.ParseWithClaims(cookie, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(config.Env("JWT_SECRET")), nil
	})

	if err != nil {
		return model, err
	}

	claims := token.Claims.(*jwt.StandardClaims)

	id, _ := strconv.ParseInt(claims.Issuer, 10, 64)

	model, err = service.userRepository.FindById(id)

	return model, err
}
