package controllers

import (
	"github.com/gofiber/fiber/v2"
	"golang-todo/helpers/validation"
	"golang-todo/services/user"
	"golang.org/x/crypto/bcrypt"
)

type ResetPasswordController struct {
	userService    *user.UserService
	userRepository *user.UserRepository
}

func NewResetPasswordController(userService *user.UserService, userRepository *user.UserRepository) *ResetPasswordController {
	return &ResetPasswordController{
		userService:    userService,
		userRepository: userRepository,
	}
}

func (controller *ResetPasswordController) ForgotPassword(c *fiber.Ctx) error {
	request := new(user.ForgotPasswordRequest)
	errors := validation.ParseAndValidate(c, request)
	if errors != nil {
		return c.Status(fiber.StatusUnprocessableEntity).JSON(errors)
	}

	userModel, err := controller.userRepository.FindByEmail(request.Email)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	err = controller.userService.SendResetUserPassword(&userModel)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	return c.JSON(fiber.Map{
		"message": "success",
	})
}

func (controller *ResetPasswordController) VerifyPasswordReset(c *fiber.Ctx) error {
	token := c.Params("token")
	if token == "" {
		return c.Status(400).JSON(fiber.Map{
			"message": "Token required!",
		})
	}

	request := new(user.VerifyChangePasswordRequest)
	errors := validation.ParseAndValidate(c, request)
	if errors != nil {
		return c.Status(fiber.StatusUnprocessableEntity).JSON(errors)
	}

	userModel, err := controller.userRepository.FindByResetPasswordToken(token)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	userModel.Password, _ = bcrypt.GenerateFromPassword([]byte(request.Password), 14)
	userModel.PasswordResetToken = ""

	_, err = controller.userRepository.Save(userModel)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	return c.JSON(nil)
}
