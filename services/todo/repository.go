package todo

import (
	"errors"
	"github.com/jinzhu/gorm"
	"golang-todo/helpers/pagination"
	"golang-todo/models"
	"time"
)

type TodoRepository struct {
	database *gorm.DB
}

type CreateTodoRequest struct {
	Name        string `json:"name" validate:"required"`
	Description string `json:"description"`
}

type UpdateTodoRequest struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Status      string `json:"status" validate:"omitempty,oneof=pending progress done"`
}

func NewTodoRepository(database *gorm.DB) *TodoRepository {
	return &TodoRepository{
		database: database,
	}
}

func (repository *TodoRepository) FindAll(user models.User, pag pagination.Pagination) (pagination.Pagination, error) {
	var todos []models.Todo
	var total int64
	offset := (pag.Page - 1) * pag.PerPage
	query := repository.database.Model(&models.Todo{}).Where("author_id = ?", user.ID)

	err := query.Count(&total).Error

	err = query.Limit(pag.PerPage).Offset(offset).Find(&todos).Error

	pag.Data = todos
	pag.Total = total

	return pag, err
}

func (repository *TodoRepository) FindOne(user models.User, id int) (models.Todo, error) {
	var todo models.Todo
	err := repository.database.Where("author_id = ?", user.ID).Find(&todo, id).Error
	if todo.ID == 0 {
		err = errors.New("Todo not found")
	}

	return todo, err
}

func (repository *TodoRepository) Create(author models.User, dto CreateTodoRequest) (models.Todo, error) {
	todo := models.Todo{
		Name:        dto.Name,
		Description: dto.Description,
		Status:      models.PENDING,
		AuthorID:    author.ID,
	}
	err := repository.database.Create(&todo).Error
	if err != nil {
		return todo, err
	}

	return todo, nil
}

func (repository *TodoRepository) Save(todo models.Todo, dto UpdateTodoRequest) (models.Todo, error) {
	var err error
	updates := make(map[string]interface{})
	if dto.Name != "" {
		updates["name"] = dto.Name
	}

	if dto.Description != "" {
		updates["description"] = dto.Description
	}

	if dto.Status != "" {
		updates["status"] = dto.Status
	}

	if len(updates) != 0 {
		updates["updated_at"] = time.Now()
		err = repository.database.Model(&todo).Updates(updates).Error
	}

	return todo, err
}

func (repository *TodoRepository) Delete(id int) int64 {
	count := repository.database.Delete(&models.Todo{}, id).RowsAffected

	return count
}
