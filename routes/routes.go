package routes

import (
	"github.com/gofiber/fiber/v2"
	jwtware "github.com/gofiber/jwt/v3"
	"github.com/jinzhu/gorm"
	"golang-todo/config"
	"golang-todo/controllers"
)

func Setup(router fiber.Router, db *gorm.DB) {
	userController, todoController, authController, resetPasswordController := controllers.Register(db)
	jwtSecret := config.Env("JWT_SECRET")

	authRouter := router.Group("/auth")
	authRouter.Post("/register", authController.Register)
	authRouter.Post("/login", authController.Login)
	authRouter.Post("/logout", authController.Logout)
	authRouter.Post("/forgot-password", resetPasswordController.ForgotPassword)
	authRouter.Post("/forgot-password/verify/:token", resetPasswordController.VerifyPasswordReset)

	userRouter := router.Group("/me")
	userRouter.Use(jwtware.New(jwtware.Config{
		SigningKey: []byte(jwtSecret),
	}))

	userRouter.Get("/", userController.GetCurrentUser)
	//userRouter.Put("/", todoController.Update)

	todoRouter := router.Group("/todos")
	todoRouter.Use(jwtware.New(jwtware.Config{
		SigningKey: []byte(jwtSecret),
	}))

	todoRouter.Get("/", todoController.GetAll)
	todoRouter.Get("/:id", todoController.Get)
	todoRouter.Put("/:id", todoController.Update)
	todoRouter.Post("/", todoController.Create)
	todoRouter.Delete("/:id", todoController.Delete)
}
