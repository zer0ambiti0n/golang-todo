package validation

import (
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

type ErrorResponse struct {
	FailedField string `json:"failedField"`
	Tag         string `json:"tag"`
	Value       string `json:"value"`
}

var validate = validator.New()

func ValidateStruct(request interface{}) []*ErrorResponse {
	var errors []*ErrorResponse
	err := validate.Struct(request)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			var element ErrorResponse
			element.FailedField = err.StructNamespace()
			element.Tag = err.Tag()
			element.Value = err.Param()
			errors = append(errors, &element)
		}
	}

	return errors
}

func ParseBody(c *fiber.Ctx, body interface{}) []*ErrorResponse {
	if err := c.BodyParser(body); err != nil {
		var errors []*ErrorResponse
		errElement := &ErrorResponse{
			FailedField: "ALL_BODY",
			Tag:         err.Error(),
		}

		errors = append(errors, errElement)

		return errors
	}

	return nil
}

func ParseAndValidate(c *fiber.Ctx, body interface{}) []*ErrorResponse {
	if err := ParseBody(c, body); err != nil {
		return err
	}

	return ValidateStruct(body)
}
