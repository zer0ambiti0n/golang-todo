package user

import (
	"errors"
	"github.com/jinzhu/gorm"
	"golang-todo/database"
	"golang-todo/models"
	"golang.org/x/crypto/bcrypt"
	"time"
)

type UserRepository struct {
	database *gorm.DB
}

func NewUserRepository(database *gorm.DB) *UserRepository {
	return &UserRepository{
		database: database,
	}
}

func (repository *UserRepository) Create(user models.User) (models.User, error) {
	user.Password, _ = bcrypt.GenerateFromPassword([]byte(user.Password), 14)

	err := repository.database.Create(&user).Error
	if err != nil {
		return user, err
	}

	return user, nil
}

func (repository *UserRepository) FindByEmail(email string) (models.User, error) {
	var user models.User

	err := database.DB.Where("email = ?", email).First(&user).Error
	if user.ID == 0 {
		err = errors.New("User not found!")
	}

	return user, err
}

func (repository *UserRepository) FindByResetPasswordToken(token string) (models.User, error) {
	var user models.User

	err := database.DB.Where("password_reset_token = ? AND password_reset_token_expires_at > ?", token, time.Now()).First(&user).Error
	if user.ID == 0 {
		err = errors.New("User not found by token!")
	}

	return user, err
}

func (repository *UserRepository) FindById(id int64) (models.User, error) {
	var user models.User

	err := database.DB.Where("id = ?", id).First(&user).Error
	if user.ID == 0 {
		err = errors.New("User not found!")
	}

	return user, err
}

func (repository *UserRepository) Save(user models.User) (models.User, error) {
	err := repository.database.Save(user).Error

	return user, err
}
