package models

import (
	"time"
)

const (
	PENDING  = "pending"
	PROGRESS = "progress"
	DONE     = "done"
)

type Todo struct {
	ID          uint       `gorm:"primary_key" json:"id"`
	CreatedAt   time.Time  `json:"createdAt"`
	UpdatedAt   time.Time  `json:"updatedAt"`
	DeletedAt   *time.Time `sql:"index" json:"deletedAt"`
	Name        string     `gorm:"Not Null" json:"name"`
	Description string     `json:"description"`
	Status      string     `gorm:"Not Null" json:"status" validate:"oneof=pending progress done"`
	AuthorID    uint       `json:"authorId"`
}
