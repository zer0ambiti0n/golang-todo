package controllers

import (
	"github.com/gofiber/fiber/v2"
	"golang-todo/services/auth"
	"golang-todo/services/user"
)

type UserController struct {
	repository  *user.UserRepository
	authService *auth.AuthService
}

func NewUserController(repository *user.UserRepository, authService *auth.AuthService) *UserController {
	return &UserController{
		repository:  repository,
		authService: authService,
	}
}

func (controller *UserController) GetCurrentUser(c *fiber.Ctx) error {
	authUser, err := controller.authService.GetAuthUser(c.Cookies("jwt"))

	if err != nil {
		c.Status(fiber.StatusUnauthorized)
		return c.JSON(fiber.Map{
			"message": "Unauthenticated",
		})
	}

	return c.JSON(fiber.Map{
		"data": authUser,
	})
}
