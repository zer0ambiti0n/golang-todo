package user

import (
	"github.com/thanhpk/randstr"
	"golang-todo/helpers/encode"
	"golang-todo/models"
	"golang-todo/services/email"
	"time"
)

type ForgotPasswordRequest struct {
	Email string `json:"email" validate:"required,email"`
}

type VerifyChangePasswordRequest struct {
	Password             string `json:"password" validate:"required,min=5"`
	PasswordConfirmation string `json:"passwordConfirmation" validate:"required,eqfield=Password"`
}

type UserService struct {
	repository *UserRepository
	mailer     *email.Mailer
}

func NewUserService(rep *UserRepository, mailer *email.Mailer) *UserService {
	return &UserService{
		repository: rep,
		mailer:     mailer,
	}
}

func (service *UserService) SendResetUserPassword(user *models.User) error {
	token := randstr.String(20)
	passwordResetToken := encode.Encode(token)

	user.PasswordResetToken = passwordResetToken
	user.PasswordResetTokenExpiresAt = time.Now().Add(time.Minute * 15)

	_, err := service.repository.Save(*user)
	if err != nil {
		return err
	}

	message := "Your password reset token (valid for 10min)"
	defaultData := &email.DefaultEmailData{
		Name:    user.Name,
		URL:     "localhost:5000/reset-password/verify/" + passwordResetToken,
		Subject: message,
		Message: message,
	}
	emailData := &email.ResetPasswordEmailData{
		DefaultData: *defaultData,
		ResetToken:  passwordResetToken,
	}

	err = service.mailer.SendEmailToUser(
		user,
		emailData,
		"reset_password.html",
		message,
	)

	return err
}
