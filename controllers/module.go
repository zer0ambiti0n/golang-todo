package controllers

import (
	"github.com/jinzhu/gorm"
	"golang-todo/config"
	"golang-todo/services/auth"
	"golang-todo/services/email"
	"golang-todo/services/todo"
	"golang-todo/services/user"
	"strconv"
)

func Register(database *gorm.DB) (UserController, TodoController, AuthController, ResetPasswordController) {
	smtpPort, _ := strconv.Atoi(config.Env("SMTP_PORT"))
	emailConfig := &email.EmailConfig{
		EmailFrom: config.Env("EMAIL_FROM"),
		SMTPPass:  config.Env("SMTP_PASS"),
		SMTPUser:  config.Env("SMTP_USER"),
		SMTPHost:  config.Env("SMTP_HOST"),
		SMTPPort:  smtpPort,
		Verify:    false,
	}
	mailer := email.NewMailer(emailConfig)
	userRepository := user.NewUserRepository(database)
	authService := auth.NewAuthService(userRepository)
	userService := user.NewUserService(userRepository, mailer)

	todoRepository := todo.NewTodoRepository(database)

	todoController := NewTodoController(todoRepository, authService)
	userController := NewUserController(userRepository, authService)
	authController := newAuthController(authService)
	resetPasswordController := NewResetPasswordController(userService, userRepository)

	return *userController, *todoController, *authController, *resetPasswordController
}
