package main

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"golang-todo/config"
	"golang-todo/database"
	"golang-todo/models"
	"golang-todo/routes"
	"log"
)

func main() {
	appPort := config.Env("APP_PORT")

	fmt.Println("App started at " + appPort + " port")

	http := fiber.New()
	http.Use(cors.New())

	database.ConnectDB()
	defer database.DB.Close()

	api := http.Group("/api")

	database.DB.AutoMigrate(&models.User{}, &models.Todo{})
	routes.Setup(api, database.DB)

	log.Fatal(http.Listen(":" + appPort))
}
