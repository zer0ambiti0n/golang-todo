package pagination

import (
	"github.com/gofiber/fiber/v2"
	"strconv"
)

type Pagination struct {
	PerPage int   `json:"perPage"`
	Page    int   `json:"page"`
	Total   int64 `json:"total"`
	Data    any   `json:"data"`
}

const DefaultPerPage = 10

func GeneratePaginationFromRequest(c *fiber.Ctx) Pagination {
	perPage, _ := strconv.Atoi(c.Query("perPage", strconv.Itoa(DefaultPerPage)))
	page, _ := strconv.Atoi(c.Query("page", strconv.Itoa(1)))

	return Pagination{
		PerPage: perPage,
		Page:    page,
	}
}
