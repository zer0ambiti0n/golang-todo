package email

import (
	"bytes"
	"crypto/tls"
	"github.com/k3a/html2text"
	"golang-todo/helpers/templates"
	"golang-todo/models"
	"gopkg.in/gomail.v2"
	"log"
)

type EmailConfig struct {
	EmailFrom string
	SMTPPass  string
	SMTPUser  string
	SMTPHost  string
	SMTPPort  int
	Verify    bool
}

type DefaultEmailData struct {
	Name    string
	URL     string
	Subject string
	Message string
}

type ResetPasswordEmailData struct {
	DefaultData DefaultEmailData
	ResetToken  string
}

type Mailer struct {
	emailConfig *EmailConfig
}

func NewMailer(config *EmailConfig) *Mailer {
	return &Mailer{
		emailConfig: config,
	}
}

func (service *Mailer) SendEmailToUser(user *models.User, data interface{}, emailTemp string, subject string) error {
	var body bytes.Buffer
	template, err := templates.ParseTemplateDir("templates")
	if err != nil {
		log.Fatal("Could not parse template", err)
		return err
	}

	err = template.ExecuteTemplate(&body, emailTemp, &data)
	if err != nil {
		log.Fatal("Could not execute template: ", err)
		return err
	}

	m := gomail.NewMessage()

	m.SetHeader("From", service.emailConfig.EmailFrom)
	m.SetHeader("To", user.Email)
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", body.String())
	m.AddAlternative("text/plain", html2text.HTML2Text(body.String()))

	d := gomail.NewDialer(
		service.emailConfig.SMTPHost,
		service.emailConfig.SMTPPort,
		service.emailConfig.SMTPUser,
		service.emailConfig.SMTPPass,
	)
	d.TLSConfig = &tls.Config{InsecureSkipVerify: !service.emailConfig.Verify}

	// Send Email
	if err := d.DialAndSend(m); err != nil {
		log.Fatal("Could not send email: ", err)
		return err
	}

	return nil
}
