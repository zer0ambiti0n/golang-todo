package models

import (
	"time"
)

const (
	ACTIVE   = "active"
	INACTIVE = "inactive"
)

type User struct {
	ID                          uint       `gorm:"primary_key" json:"id"`
	Email                       string     `json:"email" gorm:"uniqueIndex"`
	CreatedAt                   time.Time  `json:"createdAt"`
	UpdatedAt                   time.Time  `json:"updatedAt"`
	DeletedAt                   *time.Time `sql:"index" json:"deletedAt"`
	Name                        string     `gorm:"Not Null" json:"name"`
	Description                 string     `json:"description"`
	Status                      string     `gorm:"Not Null" json:"status"`
	Password                    []byte     `json:"-"`
	PasswordResetToken          string     `json:"-"`
	PasswordResetTokenExpiresAt time.Time  `json:"-"`
	Todos                       []Todo     `gorm:"foreignKey:AuthorID"`
}
