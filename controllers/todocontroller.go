package controllers

import (
	"github.com/gofiber/fiber/v2"
	"golang-todo/helpers/pagination"
	"golang-todo/helpers/validation"
	"golang-todo/services/auth"
	"golang-todo/services/todo"
	"strconv"
)

type TodoController struct {
	repository  *todo.TodoRepository
	authService *auth.AuthService
}

func NewTodoController(repository *todo.TodoRepository, authService *auth.AuthService) *TodoController {
	return &TodoController{
		repository:  repository,
		authService: authService,
	}
}

func (controller *TodoController) GetAll(c *fiber.Ctx) error {
	authUser, err := controller.authService.GetAuthUser(c.Cookies("jwt"))

	if err != nil {
		c.Status(fiber.StatusUnauthorized)
		return c.JSON(fiber.Map{
			"message": "Unauthenticated",
			"error":   err,
		})
	}

	pag := pagination.GeneratePaginationFromRequest(c)
	result, err := controller.repository.FindAll(authUser, pag)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{
			"message": "Server error!",
			"error":   err,
		})
	}

	return c.JSON(fiber.Map{
		"data": result,
	})
}

func (controller *TodoController) Get(c *fiber.Ctx) error {
	authUser, err := controller.authService.GetAuthUser(c.Cookies("jwt"))

	if err != nil {
		c.Status(fiber.StatusUnauthorized)
		return c.JSON(fiber.Map{
			"message": "Unauthenticated",
			"error":   err,
		})
	}

	id, err := strconv.Atoi(c.Params("id"))
	todoItem, err := controller.repository.FindOne(authUser, id)
	if err != nil {
		return c.Status(404).JSON(fiber.Map{
			"message": "Item not found!",
			"error":   err,
		})
	}

	return c.JSON(fiber.Map{
		"data": todoItem,
	})
}

func (controller *TodoController) Create(c *fiber.Ctx) error {
	authUser, err := controller.authService.GetAuthUser(c.Cookies("jwt"))

	if err != nil {
		c.Status(fiber.StatusUnauthorized)
		return c.JSON(fiber.Map{
			"message": "Unauthenticated",
			"error":   err,
		})
	}

	dto := new(todo.CreateTodoRequest)

	errors := validation.ParseAndValidate(c, dto)
	if errors != nil {
		return c.Status(fiber.StatusUnprocessableEntity).JSON(errors)
	}

	item, err := controller.repository.Create(authUser, *dto)

	if err != nil {
		return c.Status(400).JSON(fiber.Map{
			"message": "Failed creating item",
			"error":   err,
		})
	}

	return c.JSON(fiber.Map{
		"data": item,
	})
}

func (controller *TodoController) Update(c *fiber.Ctx) error {
	authUser, err := controller.authService.GetAuthUser(c.Cookies("jwt"))

	if err != nil {
		c.Status(fiber.StatusUnauthorized)
		return c.JSON(fiber.Map{
			"message": "Unauthenticated",
			"error":   err,
		})
	}

	id, err := strconv.Atoi(c.Params("id"))

	if err != nil {
		return c.Status(404).JSON(fiber.Map{
			"message": "Item not found",
			"error":   err,
		})
	}

	todoItem, err := controller.repository.FindOne(authUser, id)

	if err != nil {
		return c.Status(404).JSON(fiber.Map{
			"message": "Item not found",
			"error":   err,
		})
	}

	dto := new(todo.UpdateTodoRequest)

	errors := validation.ParseAndValidate(c, dto)
	if errors != nil {
		return c.Status(fiber.StatusUnprocessableEntity).JSON(errors)
	}

	item, err := controller.repository.Save(todoItem, *dto)

	if err != nil {
		return c.Status(400).JSON(fiber.Map{
			"message": "Error updating todoItem",
			"error":   err,
		})
	}

	return c.JSON(fiber.Map{
		"data": item,
	})
}

func (controller *TodoController) Delete(c *fiber.Ctx) error {
	id, err := strconv.Atoi(c.Params("id"))
	if err != nil {
		return c.Status(400).JSON(fiber.Map{
			"message": "Failed deleting todo",
			"err":     err,
		})
	}

	RowsAffected := controller.repository.Delete(id)
	if RowsAffected == 0 {
		return c.Status(404).JSON(fiber.Map{
			"message": "Item not found",
			"error":   err,
		})
	}

	return c.Status(204).JSON(nil)
}
