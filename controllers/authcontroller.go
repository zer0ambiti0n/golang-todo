package controllers

import (
	"github.com/gofiber/fiber/v2"
	"golang-todo/helpers/validation"
	"golang-todo/services/auth"
	"time"
)

type AuthController struct {
	service *auth.AuthService
}

func newAuthController(service *auth.AuthService) *AuthController {
	return &AuthController{
		service: service,
	}
}

func (controller *AuthController) Register(c *fiber.Ctx) error {
	request := new(auth.RegisterRequest)
	errors := validation.ParseAndValidate(c, request)
	if errors != nil {
		return c.Status(fiber.StatusUnprocessableEntity).JSON(errors)
	}

	user, err := controller.service.Register(*request)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	token, exp, err := controller.service.CreateJWTToken(user, c)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	return c.JSON(fiber.Map{"token": token, "exp": exp, "user": user})
}

func (controller *AuthController) Login(c *fiber.Ctx) error {
	request := new(auth.LoginRequest)
	errors := validation.ParseAndValidate(c, request)
	if errors != nil {
		return c.Status(fiber.StatusUnprocessableEntity).JSON(errors)
	}

	user, err := controller.service.Login(*request)
	if err != nil {
		return c.Status(fiber.StatusUnprocessableEntity).JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	token, exp, err := controller.service.CreateJWTToken(user, c)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	return c.JSON(fiber.Map{"token": token, "exp": exp, "user": user})
}

func (controller *AuthController) Logout(c *fiber.Ctx) error {
	cookie := fiber.Cookie{
		Name:     "jwt",
		Value:    "",
		Expires:  time.Now().Add(-time.Hour),
		HTTPOnly: true,
	}

	c.Cookie(&cookie)

	return c.JSON(fiber.Map{
		"message": "success",
	})
}
